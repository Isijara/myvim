set encoding=utf-8
let g:rehash256 = 1
colorscheme molokai
execute pathogen#infect()
filetype plugin indent on
set nocompatible
set backspace=indent,eol,start
set nu
set laststatus=2
set tabstop=4
set incsearch
set hlsearch
set expandtab
set hidden
set mouse=a
set sw=4
set nobackup
set noswapfile
syntax on
